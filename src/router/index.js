import { createRouter, createWebHashHistory } from "vue-router";
const routes = [
  //   {
  //     path: "/lowcode",
  //     name: "活动生成",
  //     meta: {
  //       title: "活动生成",
  //       icon: "Calendar",
  //     },
  //     component: () => import("@/pages/lowCode/index.vue"),
  //   },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});
export { router, routes };
